﻿using System.Data.Common;
using System.Data.Entity;
using Abp.EntityFramework;
using Prexco.Directors;
using Prexco.Movies;

namespace Prexco.EntityFramework
{
    public class PrexcoDbContext : AbpDbContext
    {
        //TODO: Define an IDbSet for each Entity...

        //Example:
        //public virtual IDbSet<User> Users { get; set; }
        public virtual IDbSet<Director> Directores { get; set; }
        public virtual IDbSet<Pelicula> Peliculas { get; set; }

        /* NOTE: 
         *   Setting "Default" to base class helps us when working migration commands on Package Manager Console.
         *   But it may cause problems when working Migrate.exe of EF. If you will apply migrations on command line, do not
         *   pass connection string name to base classes. ABP works either way.
         */
        public PrexcoDbContext()
            : base("Default")
        {

        }

        /* NOTE:
         *   This constructor is used by ABP to pass connection string defined in PrexcoDataModule.PreInitialize.
         *   Notice that, actually you will not directly create an instance of PrexcoDbContext since ABP automatically handles it.
         */
        public PrexcoDbContext(string nameOrConnectionString)
            : base(nameOrConnectionString)
        {

        }

        //This constructor is used in tests
        public PrexcoDbContext(DbConnection existingConnection)
         : base(existingConnection, false)
        {

        }

        public PrexcoDbContext(DbConnection existingConnection, bool contextOwnsConnection)
         : base(existingConnection, contextOwnsConnection)
        {

        }
    }
}
