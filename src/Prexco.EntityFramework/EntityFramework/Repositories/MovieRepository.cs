﻿using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using Abp.EntityFramework;
using Prexco.Movies;

namespace Prexco.EntityFramework.Repositories
{
    public class MovieRepository : PrexcoRepositoryBase<Pelicula>, IMovieRepository
    {
        public MovieRepository(
            IDbContextProvider<PrexcoDbContext> dbContextProvider
        ) : base(dbContextProvider)
        {
        }

        public IQueryable<Pelicula> GetBestMovieFromDirector(int directorId) => GetAll()
            .Where(p => p.DirectorId == directorId)
            .OrderByDescending(p => p.Clasificacion)
            .Take(() => 1);

        public IQueryable<Pelicula> GetWorstMovieFromDirector(int directorId) => GetAll()
            .Where(p => p.DirectorId == directorId)
            .OrderBy(p => p.Clasificacion)
            .Take(() => 1);

        public async Task<double?> GetAverageMoviesFromDirector(int directorId) => await GetAll()
            .Where(p => p.DirectorId == directorId)
            .AverageAsync(p => (double?) p.Clasificacion);
    }
}