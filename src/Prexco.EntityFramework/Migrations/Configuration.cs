using System.Data.Entity.Migrations;

namespace Prexco.Migrations
{
    internal sealed class Configuration : DbMigrationsConfiguration<Prexco.EntityFramework.PrexcoDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = true;
            ContextKey = "Prexco";
        }

        protected override void Seed(Prexco.EntityFramework.PrexcoDbContext context)
        {
            // This method will be called every time after migrating to the latest version.
            // You can add any seed data here...
        }
    }
}
