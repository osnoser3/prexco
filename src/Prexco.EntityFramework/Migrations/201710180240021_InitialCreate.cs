namespace Prexco.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitialCreate : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Directors",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Nombre = c.String(),
                        Apellidos = c.String(),
                        Nacimiento = c.DateTime(nullable: false),
                        Sexo = c.Int(nullable: false),
                        DeletedAt = c.DateTime(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Peliculas",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Nombre = c.String(),
                        DirectorId = c.Int(nullable: false),
                        Duracion = c.Int(nullable: false),
                        Clasificacion = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Directors", t => t.DirectorId, cascadeDelete: true)
                .Index(t => t.DirectorId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Peliculas", "DirectorId", "dbo.Directors");
            DropIndex("dbo.Peliculas", new[] { "DirectorId" });
            DropTable("dbo.Peliculas");
            DropTable("dbo.Directors");
        }
    }
}
