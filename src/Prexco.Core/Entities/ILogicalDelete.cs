﻿using System;
using JetBrains.Annotations;

namespace Prexco.Entities
{
    public interface ILogicalDelete
    {
        DateTime? DeletedAt { get; set; }
    }
}