﻿using System.Linq;

namespace Prexco.Entities
{
    public static class EntityAppExtensions
    {
        public static IQueryable<T> ExcludeLogicallyDeleted<T>(this IQueryable<T> query) where T : class, ILogicalDelete =>
            query.Where(t => !t.DeletedAt.HasValue);
    }
}