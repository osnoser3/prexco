﻿using System.Linq;
using System.Threading.Tasks;
using Abp.Domain.Repositories;

namespace Prexco.Movies
{
    public interface IMovieRepository : IRepository<Pelicula>
    {
        Task<double?> GetAverageMoviesFromDirector(int directorId);
        IQueryable<Pelicula> GetBestMovieFromDirector(int directorId);
        IQueryable<Pelicula> GetWorstMovieFromDirector(int directorId);
    }
}