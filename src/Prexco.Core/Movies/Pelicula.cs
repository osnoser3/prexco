﻿using Abp.Domain.Entities;
using Prexco.Directors;

namespace Prexco.Movies
{
    public class Pelicula : Entity
    {
        public string Nombre { get; set; }
        public int DirectorId { get; set; }
        public int Duracion { get; set; }
        public int Clasificacion { get; set; }

        public virtual Director Director { get; set; }
    }
}