﻿// ReSharper disable VirtualMemberCallInConstructor

using System;
using System.Collections.Generic;
using Abp.Domain.Entities;
using Prexco.Entities;
using Prexco.Movies;

namespace Prexco.Directors
{
    public class Director: Entity, ILogicalDelete
    {
        public string Nombre { get; set; }
        public string Apellidos { get; set; }
        public DateTime Nacimiento { get; set; }
        public Sexo Sexo { get; set; }
        public DateTime? DeletedAt { get; set; }

        public virtual ICollection<Pelicula> Peliculas { get; set; }

        public Director()
        {
            Peliculas = new List<Pelicula>();
        }
    }
}