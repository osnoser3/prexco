﻿using System.Reflection;
using Abp.Modules;

namespace Prexco
{
    public class PrexcoCoreModule : AbpModule
    {
        public override void Initialize()
        {
            IocManager.RegisterAssemblyByConvention(Assembly.GetExecutingAssembly());
        }
    }
}
