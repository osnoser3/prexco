﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Abp.WebApi.Controllers;
using Prexco.Movies;
using Prexco.Movies.Dtos;

namespace Prexco.Controllers
{
    public class MoviesController : AbpApiController
    {
        private readonly MovieAppService _movieService;

        public MoviesController(MovieAppService movieService)
        {
            _movieService = movieService;
        }

        public Task<List<PeliculaDto>> GetAll(string populate = null) => _movieService.GetAll(
            populate?.Split(',').Select(p => p.First().ToString().ToUpper() + p.Substring(1)).ToArray()
        );

        public Task<PeliculaDto> Post(PeliculaDto movie) => _movieService.Create(movie);

    }
}