﻿using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web.Http;
using Abp.WebApi.Controllers;
using Prexco.Directors;
using Prexco.Directors.Dtos;

namespace Prexco.Controllers
{
    [RoutePrefix("api/directors")]
    public class DirectorsController : AbpApiController
    {
        private readonly DirectorAppService _directorService;

        public DirectorsController(DirectorAppService directorService)
        {
            _directorService = directorService;
        }

        public async Task<IHttpActionResult> Get(int id)
        {
            var director = await _directorService.Get(id);
            return director != null ? (IHttpActionResult) Ok(director) : NotFound();
        }

        public async Task Delete(int id) => await _directorService.Delete(id);

        public Task<List<DirectorDto>> GetAll() => _directorService.GetAll();

        [Route("{id:int}/movie-stats", Name = "GetMovieStats")]
        public Task<DirectorMovieStatsDto> GetMovieStats(int id) => _directorService.GetMovieStats(id);

        public Task<DirectorDto> Post(DirectorDto director) => _directorService.Create(director);

        public async Task<IHttpActionResult> Patch(int id, DirectorDto director)
        {
            if (id == director.Id)
            {
                BadRequest();
            }

            director = await _directorService.Update(director);
            return director != null ? (IHttpActionResult) Ok(director) : NotFound();
        }
    }
}