﻿using Abp.Web.Mvc.Views;

namespace Prexco.Web.Views
{
    public abstract class PrexcoWebViewPageBase : PrexcoWebViewPageBase<dynamic>
    {

    }

    public abstract class PrexcoWebViewPageBase<TModel> : AbpWebViewPage<TModel>
    {
        protected PrexcoWebViewPageBase()
        {
            LocalizationSourceName = PrexcoConsts.LocalizationSourceName;
        }
    }
}