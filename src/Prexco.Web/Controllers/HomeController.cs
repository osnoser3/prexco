﻿using System.Web.Mvc;

namespace Prexco.Web.Controllers
{
    public class HomeController : PrexcoControllerBase
    {
        public ActionResult Index()
        { 
            return View("~/App/Main/views/layout/layout.cshtml"); //Layout of the angular application.
        }
	}
}