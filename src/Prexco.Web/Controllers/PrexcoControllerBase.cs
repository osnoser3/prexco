﻿using Abp.Web.Mvc.Controllers;

namespace Prexco.Web.Controllers
{
    /// <summary>
    /// Derive all Controllers from this class.
    /// </summary>
    public abstract class PrexcoControllerBase : AbpController
    {
        protected PrexcoControllerBase()
        {
            LocalizationSourceName = PrexcoConsts.LocalizationSourceName;
        }
    }
}