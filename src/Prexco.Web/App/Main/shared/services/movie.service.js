﻿class MovieService {

    constructor($http) {
        this.http = $http;
        this.url = `api/movies`;
    }

    create(movie) {
        return this.http.post(this.url, movie)
            .then(d => d.data);
    }

    getAll() {
        return this.http.get(`${this.url}?populate=director`)
            .then(d => d.data || []);
    }

}

angular.module('shared').service('movieService', [
    '$http',
    MovieService,
]);