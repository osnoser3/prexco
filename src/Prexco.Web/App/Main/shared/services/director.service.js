﻿class DirectorService {
    constructor($http) {
        this.http = $http;
        this.url = `api/directors`;
    }

    create(director) {
        return this.http.post(this.url, director)
            .then(d => d.data);
    }

    delete(id) {
        return this.http.delete(`${this.url}/${id}`);
    }

    get(id) {
        return this.http.get(`${this.url}/${id}`)
            .then(d => d.data);
    }

    getAll() {
        return this.http.get(this.url)
            .then(d => d.data || []);
    }

    getMovieStats(id) {
        return this.http.get(`${this.url}/${id}/movie-stats`)
            .then(d => d.data || []);
    }

    update(director) {
        return this.http.patch(`${this.url}/${director.id}`, director)
            .then(d => d.data);
    }
}

angular.module('shared').service('directorService', [
    '$http',
    DirectorService,
]);