﻿(function () {
    'use strict';

    var app = angular.module('app', [
        'ngAnimate',
        'ngSanitize',

        'ui.router',
        'ui.bootstrap',
        'ui.jq',

        'abp',

        'director',
        'movie',
    ]);

    //Configuration for Angular UI routing.
    app.config([
        '$stateProvider', '$urlRouterProvider', '$locationProvider', '$qProvider',
        function ($stateProvider, $urlRouterProvider, $locationProvider, $qProvider) {
            $locationProvider.hashPrefix('');
            $urlRouterProvider.otherwise('/');
            $qProvider.errorOnUnhandledRejections(false);

            $stateProvider
                .state('home', {
                    url: '/',
                    templateUrl: '/App/Main/views/home/home.cshtml',
                    menu: 'Home' //Matches to name of 'Home' menu in PrexcoNavigationProvider
                })
                .state('about', {
                    url: '/about',
                    templateUrl: '/App/Main/views/about/about.cshtml',
                    menu: 'About' //Matches to name of 'About' menu in PrexcoNavigationProvider
                })
                .state('directors', {
                    url: '/directors',
                    template: '<ui-view></ui-view>',
                })
                .state('directors.list', {
                    url: '/',
                    template: '<director-list></director-list>',
                    menu: 'Directors' //Matches to name of 'About' menu in PrexcoNavigationProvider
                })
                .state('directors.new', {
                    url: '/new',
                    template: '<director-form></director-form>',
                    menu: 'Directors' //Matches to name of 'About' menu in PrexcoNavigationProvider
                })
                .state('directors.edit', {
                    url: '/{id}',
                    template: '<director-form></director-form>',
                    menu: 'Directors' //Matches to name of 'About' menu in PrexcoNavigationProvider
                })
                .state('directors.movie-stats', {
                    url: '/{id}/movie-stats',
                    template: '<director-movie-stats-detail></director-movie-stats-detail>',
                    menu: 'Directors' //Matches to name of 'About' menu in PrexcoNavigationProvider
                })
                .state('movies', {
                    url: '/movies',
                    template: '<ui-view></ui-view>',
                })
                .state('movies.list', {
                    url: '/',
                    template: '<movie-list></movie-list>',
                    menu: 'Movies' //Matches to name of 'About' menu in PrexcoNavigationProvider
                })
                .state('movies.new', {
                    url: '/new',
                    template: '<movie-form></movie-form>',
                    menu: 'Movies' //Matches to name of 'About' menu in PrexcoNavigationProvider
                });
        }
    ]);
})();