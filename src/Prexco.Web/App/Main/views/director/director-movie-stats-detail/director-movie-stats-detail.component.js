﻿class DirectorMovieStatsDetailComponent {

    constructor(directorService, $stateParams) {
        this.directorService = directorService;
        this.id = $stateParams.id;
    }

    $onInit() {
        this.getMovieStatsFromApi(this.id)
            .then(stats => this.stats = stats, _ => {});
    }

    getMovieStatsFromApi(directorId) {
        return this.directorService.getMovieStats(directorId);
    } 

}

angular.module('director').component('directorMovieStatsDetail', {
    templateUrl: '/App/Main/views/director/director-movie-stats-detail/director-movie-stats-detail.cshtml',
    controller: [
        'directorService',
        '$stateParams',
        DirectorMovieStatsDetailComponent,
    ],
});