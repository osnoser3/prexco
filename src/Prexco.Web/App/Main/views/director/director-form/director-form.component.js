﻿class DirectorFormComponent {

    constructor(directorService, $state, $stateParams) {
        this.directorService = directorService;
        this.params = $stateParams;
        this.router = $state;
        this.loading = false;
    }

    $onInit() {
        const id = this.params.id;
        if (!id) { return; }

        this.loading = true;
        this.getDirectorFromApi(id)
            .then(d => this.setModel(d))
            .then(_ => this.loading = false, _ => this.loading = false);
    }

    getDirectorFromApi(id) {
        return this.directorService.get(id);
    } 

    onSubmit() {
        this.loading = true;
        const result = !this.original
            ? this.directorService.create(this.model)
            : this.directorService.update(this.model);

        result.then(_ => {
            abp.notify.success('Data saved successfully!');
            this.router.go('directors.list');
        }, _ => this.loading = false);
    }

    setModel(director) {
        this.original = { ...director };
        director.nacimiento = new Date(director.nacimiento);
        this.model = director;
    }
}

angular.module('director').component('directorForm', {
    templateUrl: '/App/Main/views/director/director-form/director-form.cshtml',
    controller: [
        'directorService',
        '$state',
        '$stateParams',
        DirectorFormComponent,
    ],
});