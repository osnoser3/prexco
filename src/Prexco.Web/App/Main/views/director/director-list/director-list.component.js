﻿class DirectorListComponent {

    constructor(directorService) {
        this.directorService = directorService;
        this.directors = [];
    }

    $onInit() {
        this.getDirectorsFromApi()
            .then(d => this.directors = d);
    }

    confirmDelete(director) {
        const result = confirm('Are you sure you want to delete the selected item?');
        if (!result) { return; }

        this.directorService.delete(director.id)
            .then(_ => this.getDirectorsFromApi())
            .then(d => this.directors = d);
    }

    getDirectorsFromApi() {
        return this.directorService.getAll().catch(_ => {});
    }

}

angular.module('director').component('directorList', {
    templateUrl: '/App/Main/views/director/director-list/director-list.cshtml',
    controller: [
        'directorService',
        DirectorListComponent,
    ],
});
