﻿class MovieFormComponent {

    constructor(directorService, movieService, $state) {
        this.directorService = directorService;
        this.movieService = movieService;
        this.router = $state;
        this.directors = [];
        this.loading = false;
    }

    $onInit() {
        this.getDirectorsFromApi()
            .then(d => this.directors = d);
    }

    getDirectorsFromApi() {
        return this.directorService.getAll();
    }

    getMovieFromApi(id) {
        return this.movieService.get(id);
    } 

    onSubmit() {
        this.loading = true;
        const result = !this.original
            ? this.movieService.create(this.model)
            : this.movieService.update(this.model);

        result.then(_ => {
            abp.notify.success('Data saved successfully!');
            this.router.go('movies.list');
        }, _ => this.loading = false);
    }

    setModel(movie) {
        movie.nacimiento = new Date(movie.nacimiento);
        this.model = movie;
    }
}

angular.module('movie').component('movieForm', {
    templateUrl: '/App/Main/views/movie/movie-form/movie-form.cshtml',
    controller: [
        'directorService',
        'movieService',
        '$state',
        MovieFormComponent,
    ],
});