﻿class MovieListComponent {

    constructor(movieService) {
        this.movieService = movieService;
        this.movies = [];
    }

    $onInit() {
        this.getMoviesFromApi()
            .then(d => this.movies = d, _ => {});
    }

    getMoviesFromApi() {
        return this.movieService.getAll();
    }

}

angular.module('movie').component('movieList', {
    templateUrl: '/App/Main/views/movie/movie-list/movie-list.cshtml',
    controller: [
        'movieService',
        MovieListComponent,
    ],
});
