﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Abp.Application.Services;
using Prexco.Movies.Dtos;

namespace Prexco.Movies
{
    public interface IMovieAppService : IApplicationService
    {
        Task<List<PeliculaDto>> GetAll(string[] populate = null);
        Task<PeliculaDto> Create(PeliculaDto pelicula);
    }
}