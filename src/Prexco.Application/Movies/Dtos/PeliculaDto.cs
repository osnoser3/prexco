﻿namespace Prexco.Movies.Dtos
{
    public class PeliculaDto
    {
        public int? Id { get; set; }
        public string Nombre { get; set; }
        public int DirectorId { get; set; }
        public int Duracion { get; set; }
        public int Clasificacion { get; set; }

        public DirectorPeliculaDto Director { get; set; }
    }

    public class DirectorPeliculaDto
    {
        public string Nombre { get; set; }
    }
}