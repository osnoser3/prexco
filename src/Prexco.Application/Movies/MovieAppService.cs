﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Domain.Repositories;
using Abp.UI;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using Prexco.Directors;
using Prexco.Movies.Dtos;

namespace Prexco.Movies
{
    public class MovieAppService : ApplicationService, IMovieAppService
    {
        private readonly IRepository<Director> _directorRepository;
        private readonly IMapper _mapper;
        private readonly IMovieRepository _peliculaRepository;

        public MovieAppService(
            IMapper mapper,
            IMovieRepository peliculaRepository,
            IRepository<Director> directorRepository
        )
        {
            _mapper = mapper;
            _peliculaRepository = peliculaRepository;
            _directorRepository = directorRepository;
        }

        public Task<List<PeliculaDto>> GetAll(string[] populate)
        {
            var query = _peliculaRepository.GetAll();

            return (populate != null
                ? query.ProjectTo<PeliculaDto>(_mapper.ConfigurationProvider, null, populate)
                : query.ProjectTo<PeliculaDto>(_mapper.ConfigurationProvider)
            ).ToListAsync();
        }

        public async Task<PeliculaDto> Create(PeliculaDto pelicula)
        {
            CreateValidate(pelicula);

            var peliculaModel = _mapper.Map<Pelicula>(pelicula);
            peliculaModel.Id = await _peliculaRepository.InsertAndGetIdAsync(peliculaModel);

            return _mapper.Map<PeliculaDto>(peliculaModel);
        }

        private void CreateValidate(PeliculaDto pelicula)
        {
            if (pelicula.Id.HasValue && PeliculaExists(pelicula.Id.Value))
                throw new UserFriendlyException("There is already a movie with given id");

            // ReSharper disable once PossibleInvalidOperationException
            if (!DirectorExist(pelicula.DirectorId))
                throw new UserFriendlyException("The movie does not exist");
        }

        private bool PeliculaExists(int id) => _peliculaRepository.GetAll().Any(d => d.Id == id);

        private bool DirectorExist(int id) => _directorRepository.GetAll().Any(d => d.Id == id && !d.DeletedAt.HasValue);
    }
}