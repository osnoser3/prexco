﻿using Prexco.Movies.Dtos;

namespace Prexco.Directors.Dtos
{
    public class DirectorMovieStatsDto
    {
        public PeliculaDto BestMovie { get; set; }
        public PeliculaDto WorstMovie { get; set; }
        public double? MovieRatingProm{ get; set; }
    }
}