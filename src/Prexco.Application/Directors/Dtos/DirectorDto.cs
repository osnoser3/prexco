﻿using System;

namespace Prexco.Directors.Dtos
{
    public class DirectorDto
    {
        public int? Id { get; set; }
        public string Nombre { get; set; }
        public string Apellidos { get; set; }
        public DateTime? Nacimiento { get; set; }
        public Sexo Sexo { get; set; }
    }
}