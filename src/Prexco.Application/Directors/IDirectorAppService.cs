﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Abp.Application.Services;
using Prexco.Directors.Dtos;

namespace Prexco.Directors
{
    public interface IDirectorAppService : IApplicationService
    {
        Task<DirectorDto> Get(int id);
        Task<DirectorMovieStatsDto> GetMovieStats(int directorId);
        Task<List<DirectorDto>> GetAll();
        Task<DirectorDto> Create(DirectorDto director);
        Task<DirectorDto> Update(DirectorDto director);
        Task<bool> Delete(int id);
    }
}