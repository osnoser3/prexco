﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Domain.Repositories;
using Abp.UI;
using AutoMapper;
using Prexco.Directors.Dtos;
using Prexco.Entities;
using Prexco.Movies;
using Prexco.Movies.Dtos;

namespace Prexco.Directors
{
    public class DirectorAppService : ApplicationService, IDirectorAppService
    {
        private readonly IMapper _mapper;
        private readonly IRepository<Director> _directorRepository;
        private readonly IMovieRepository _movieRepository;

        public DirectorAppService(
            IMapper mapper,
            IRepository<Director> directorRepository,
            IMovieRepository movieRepository
        )
        {
            _mapper = mapper;
            _directorRepository = directorRepository;
            _movieRepository = movieRepository;
        }

        public Task<DirectorDto> Get(int id) => _directorRepository.GetAll()
            .Where(d => d.Id == id)
            .ExcludeLogicallyDeleted()
            .ProjectToFirstOrDefaultAsync<DirectorDto>(_mapper.ConfigurationProvider);

        public async Task<DirectorMovieStatsDto> GetMovieStats(int directorId)
        {
            if (!DirectorExists(directorId)) return null;

            return new DirectorMovieStatsDto
            {
                BestMovie = await _movieRepository.GetBestMovieFromDirector(directorId)
                    .ProjectToFirstOrDefaultAsync<PeliculaDto>(_mapper.ConfigurationProvider),
                WorstMovie = await _movieRepository.GetWorstMovieFromDirector(directorId)
                    .ProjectToFirstOrDefaultAsync<PeliculaDto>(_mapper.ConfigurationProvider),
                MovieRatingProm = await _movieRepository.GetAverageMoviesFromDirector(directorId)
            };
        }

        public Task<List<DirectorDto>> GetAll() => _directorRepository.GetAll()
            .ExcludeLogicallyDeleted()
            .ProjectToListAsync<DirectorDto>(_mapper.ConfigurationProvider);

        public async Task<DirectorDto> Create(DirectorDto director)
        {
            if (director.Id.HasValue && DirectorExists(director.Id.Value))
            {
                throw new UserFriendlyException("There is already a director with given id");
            }

            var directorModel = _mapper.Map<Director>(director);
            directorModel.Id = await _directorRepository.InsertAndGetIdAsync(directorModel);

            return _mapper.Map<DirectorDto>(directorModel);
        }

        public async Task<DirectorDto> Update(DirectorDto director)
        {
            var directorModel = _mapper.Map<Director>(director);
            directorModel = await _directorRepository.UpdateAsync(directorModel);

            return _mapper.Map<DirectorDto>(directorModel);
        }

        public async Task<bool> Delete(int directorId)
        {
            var director = _directorRepository.FirstOrDefault(directorId);
            if (director == null) return false;

            if (director.Peliculas.Count == 0)
            {
                await _directorRepository.DeleteAsync(directorId);
            }
            else
            {
                director.DeletedAt = DateTime.Now;
            }

            return true;
        }

        private bool DirectorExists(int id) => _directorRepository.GetAll().Any(d => d.Id == id);
    }
}