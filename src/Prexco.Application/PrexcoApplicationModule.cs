﻿using System.Reflection;
using Abp.AutoMapper;
using Abp.Modules;
using Prexco.Directors;
using Prexco.Directors.Dtos;
using Prexco.Movies;
using Prexco.Movies.Dtos;

namespace Prexco
{
    [DependsOn(typeof(PrexcoCoreModule), typeof(AbpAutoMapperModule))]
    public class PrexcoApplicationModule : AbpModule
    {
        public override void PreInitialize()
        {
            Configuration.Modules.AbpAutoMapper().Configurators.Add(config =>
            {
                config.CreateMap<Director, DirectorDto>().ReverseMap();
                config.CreateMap<Pelicula, PeliculaDto>().ReverseMap();
                config.CreateMap<Pelicula, PeliculaDto>()
                    .ForMember(dto => dto.Director, opt =>
                    {
                        opt.MapFrom(p => p.Director);
                        opt.ExplicitExpansion();
                    })
                    .ReverseMap();
                config.CreateMap<Director, DirectorPeliculaDto>()
                    .ForMember(dto => dto.Nombre, opt => opt.MapFrom(d => d.Nombre + " " + d.Apellidos))
                    .ReverseMap()
                    .ForMember(d => d.Nombre, opt => opt.Ignore());
            });
        }
        public override void Initialize()
        {
            IocManager.RegisterAssemblyByConvention(Assembly.GetExecutingAssembly());
        }
    }
}
